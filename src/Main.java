import java.util.Scanner;
import Instructions.InstructionFactory;
import Rooms.RobotRoom;

public class Main {

    public static void main(String[] args) {

        //Change to Rooms.CircularRoom if wanted
        RobotRoom room = new RobotRoom();
        Robot robot = new Robot(room);
        String temp;
        Scanner sc = new Scanner(System.in);
        System.out.println("Skriv in instruktion:");
        InstructionFactory iFactory = new InstructionFactory();
        while(sc.hasNextLine()){
            temp = sc.nextLine();

            robot.runRobot(iFactory.makeInstruction(temp));
        }

    }



}
