import Instructions.FakeInstruction;
import Instructions.Instruction;
import Rooms.Room;
import Assets.*;


import java.awt.*;

public class Robot extends Point {

    Direction direction;
    Room room;
    Point outsideRoom;
    public Robot(Room room){
        this.room = room;
        direction = Direction.NORTH;
        setLocation(room.getStartPosition());
    }

    private void rotateRight(){
        if(direction == Direction.NORTH){
            direction = Direction.EAST;
        } else if(direction == Direction.EAST){
            direction = Direction.SOUTH;
        }
        else if(direction == Direction.SOUTH){
            direction = Direction.WEST;
        } else {
            direction = Direction.NORTH;
        }

    }
    private void rotateleft(){
        if(direction == Direction.NORTH){
            direction = Direction.WEST;
        } else if(direction == Direction.WEST){
            direction = Direction.SOUTH;
        }
        else if(direction == Direction.SOUTH){
            direction = Direction.EAST;
        } else {
            direction = Direction.NORTH;
        }



    }
    private boolean forward(){
        int x = (int)getX();
        int y = (int)getY();
        Point temp;
        boolean acceptedPoint = false;
        switch(direction) {
            case NORTH:
                temp = new Point(x, y - 1);
                if (room.contains(temp)) {
                    move(x, y - 1);
                    acceptedPoint = true;
                }
                outsideRoom = temp;
                break;
            case EAST:
                temp = new Point(x + 1, y);
                if (room.contains(temp)) {
                    move(x + 1, y);
                    acceptedPoint = true;
                }
                outsideRoom = temp;
                break;
            case SOUTH:
                temp = new Point(x, y + 1);
                if (room.contains(temp)) {
                    move(x, y + 1);
                    acceptedPoint = true;
                }
                outsideRoom = temp;
                break;

            case WEST:
                temp = new Point(x - 1, y);
                if (room.contains(temp)) {
                    move(x - 1, y);
                    acceptedPoint = true;
                }
                outsideRoom = temp;
                break;
        }
        return acceptedPoint;
    }

    public Direction getDirection() {
        return direction;
    }

    public void runRobot(Instruction instruction){

        if (instruction instanceof FakeInstruction) {
            System.out.println(instruction.giveInstruction());
        } else {
            char[] chars = instruction.giveInstruction().toCharArray();
            for (char ch: chars) {
                if (ch == 'H') {
                    rotateRight();
                } else if (ch == 'V') {
                    rotateleft();
                } else if (ch == 'G') {
                    if(!forward()) System.out.println("Coordinate outside room: "+ outsideRoom.getX() + ", " + outsideRoom.getY());
                }
            }
           System.out.println(toString());
        }
    }

   public String toString(){
        return  x + " " + y + " "+ direction;
    }
}

