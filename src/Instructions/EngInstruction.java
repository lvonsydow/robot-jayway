package Instructions;

public class EngInstruction implements Instruction {
    String inst;
    public EngInstruction(String inst){
        this.inst = inst;
    }

    /*Translates instruction to swedish*/
    @Override
    public String giveInstruction() {
        char[] chars = inst.toCharArray();
        String temp = "";
        for (char ch: chars) {
            if (ch == 'R') {
                temp += "H";
            } else if (ch == 'L') {
                temp += "V";
            } else if (ch == 'F') {
                temp += "G";
            } else {
                return "";
            }
        }
        return temp;
    }
}
