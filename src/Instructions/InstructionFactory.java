package Instructions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InstructionFactory {
    ArrayList<Character> letters, swe, eng;

    public InstructionFactory(){
        letters = new ArrayList<Character>(Arrays.asList('H','V','G','R','L','F'));
        swe = new ArrayList<Character>(Arrays.asList('H','V','G'));
        eng = new ArrayList<Character>(Arrays.asList('R','L','F'));

    }

    /*Create instruction based on input, swedish, english or fake(if input is not accepted)*/
    public Instruction makeInstruction(String instruction) {
        if (instruction != null) {
            if (checkString(instruction, letters)) {
                if (checkString(instruction, eng)) {
                    return new EngInstruction(instruction);
                } else if (checkString(instruction, swe)) {
                    return new SweInstruction(instruction);
                }
            }
        }
        return new FakeInstruction(instruction);

    }
    /*Check input string*/
    private boolean checkString(String inst, List<Character> letters) {
        for (char ch: inst.toCharArray()) {
            if(!letters.contains(ch)){
                return false;
            }
        }

        return true;


    }

}