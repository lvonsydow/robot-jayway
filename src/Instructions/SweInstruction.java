package Instructions;

public class SweInstruction implements Instruction {
    String inst;
    public SweInstruction(String inst){
        this.inst = inst;
    }

    @Override
    public String giveInstruction() {
        return inst;
    }
}
