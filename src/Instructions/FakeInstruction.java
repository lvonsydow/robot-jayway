package Instructions;

public class FakeInstruction implements Instruction {
    String inst;


    public FakeInstruction(String inst) {
        this.inst = inst;
    }

    @Override
    public String giveInstruction() {
        if(inst == null){
            inst = "null";
        }
        return "Instruktionen: '" + inst + "' är felaktig";
    }
}