package Rooms;

import java.awt.*;

public class RobotRoom implements Room {
    int max, min;
    Point start;
    public RobotRoom(){
        this.start = new Point(1,2);
        min = 1;
        max = 5;
    }
    @Override
    public Point getStartPosition() {
        return this.start;
    }

    /*Controls the values to be in defined area*/
    @Override
    public boolean contains(Point position) {
        int tempX = (int)position.getX();
        int tempY = (int)position.getY();
        if((min <= tempX) && (min <= tempY) && (max >= tempX) && (max >= tempY)){
            return true;
        }
        return false;
    }
}
