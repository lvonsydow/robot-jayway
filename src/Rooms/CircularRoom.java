package Rooms;

import java.awt.*;

public class CircularRoom implements Room {
    int rad;
    int startX, startY;
    Point start;
    public CircularRoom(){
        this.startX = 0;
        this.startY = 0;
        this.start = new Point(startX, startY);
        this.rad = 10;

    }
    @Override
    public Point getStartPosition() {
        return this.start;
    }

    /*Controls the values to be in defined area*/
    @Override
    public boolean contains(Point position) {
        int x = (int)position.getX();
        int y = (int)position.getY();
        if((Math.pow((x-startX),2) + Math.pow((y-startY),2)) <= Math.pow(rad,2)){
            return true;
        }
        return false;
    }
}
