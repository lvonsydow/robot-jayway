import Instructions.EngInstruction;
import Instructions.FakeInstruction;
import Instructions.InstructionFactory;
import Instructions.SweInstruction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class instructionFactoryTest {

    @Test
    void makeInstructionSwe() {
        InstructionFactory factory = new InstructionFactory();
        String inst = "HVG";
        assertEquals(factory.makeInstruction(inst).getClass(), SweInstruction.class);
    }
    @Test
    void makeInstructionEng() {
        InstructionFactory factory = new InstructionFactory();
        String inst = "RLF";
        assertEquals(factory.makeInstruction(inst).getClass(), EngInstruction.class);


    }
    @Test
    void makeInstructionFake() {
        InstructionFactory factory = new InstructionFactory();
        String inst = "RLG";
        assertEquals(factory.makeInstruction(inst).getClass(), FakeInstruction.class);
    }
    @Test
    void makeInstructionWrong() {
        InstructionFactory factory = new InstructionFactory();
        String inst = "TOLPGG";
        assertEquals(factory.makeInstruction(inst).getClass(), FakeInstruction.class);
    }
    @Test
    void makeInstructionFakeNull() {
        InstructionFactory factory = new InstructionFactory();
        String inst = null;
        assertEquals(factory.makeInstruction(inst).getClass(), FakeInstruction.class);
    }
}