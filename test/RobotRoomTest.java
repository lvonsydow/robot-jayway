import Rooms.RobotRoom;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class RobotRoomTest {

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getStartPosition() {
        RobotRoom room = new RobotRoom();
        Point point = new Point(1,2);
        assertEquals(room.getStartPosition(), point);
    }

    @Test
    void containsFalse() {
        RobotRoom room = new RobotRoom();
        Point point = new Point(10,10);
        assertFalse(room.contains(point));
    }
    @Test
    void containsTrue() {
        RobotRoom room = new RobotRoom();
        Point point = new Point(5,5);
        assertTrue(room.contains(point));
    }
}