import Rooms.CircularRoom;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class CircularRoomTest {

    @Test
    void getStartPosition() {
        CircularRoom room = new CircularRoom();
        Point point = new Point(0,0);
        assertEquals(room.getStartPosition(), point);
    }

    @Test
    void containsFalse() {
        CircularRoom room = new CircularRoom();
        Point point = new Point(-10,10);
        assertFalse(room.contains(point));
    }
    @Test
    void containsTrue() {
        CircularRoom room = new CircularRoom();
        Point point = new Point(-5,3);
        assertTrue(room.contains(point));
    }
}