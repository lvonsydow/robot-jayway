import Instructions.Instruction;
import Instructions.InstructionFactory;
import Rooms.CircularRoom;
import Rooms.RobotRoom;
import Rooms.Room;
import Assets.Direction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class RobotTest {
    Room room, room2;
    Robot robot, robot2;
    InstructionFactory factory;
    @BeforeEach
    void setUp() {
        room = new CircularRoom();
        room2 = new RobotRoom();
        robot = new Robot(room);
        robot2 = new Robot(room2);
        factory = new InstructionFactory();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getDirection() {
        Instruction inst = factory.makeInstruction("HHHHVVVGG");
        Direction dir = Direction.EAST;
        robot.runRobot(inst);
        assertEquals(robot.getDirection(), dir);
    }

    @Test
    void runRobotCircular() {
        Instruction inst = factory.makeInstruction("RRFLFFLRF");
        robot.runRobot(inst);
        Point p = new Point(3, 1);
        assertEquals(robot, p);
        assertEquals(robot.getDirection(), Direction.EAST);

    }
    @Test
    void runRobot5x5() {
        Instruction inst = factory.makeInstruction("HGHGGHGHG");
        robot2.runRobot(inst);
        Point p = new Point(1, 3);
        assertEquals(robot2, p);
        assertEquals(robot2.getDirection(), Direction.NORTH);

    }

    @Test
    void robotToString() {
        Instruction inst = factory.makeInstruction("RRFLFFLRF");
        robot.runRobot(inst);
        Point p = new Point(3, 1);
        assertEquals(robot, p);
    }
}