import Instructions.Instruction;
import Instructions.SweInstruction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SweInstructionTest {

    @Test
    void giveInstruction() {
        String testString ="HHVHGG";
       Instruction inst = new SweInstruction(testString);
        assertEquals(inst.giveInstruction(), testString);

    }
}