import Instructions.EngInstruction;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EngInstructionTest {

    @Test
    void giveInstruction() {
        String inst = "LRLRLRLRLFFF";
        EngInstruction engInst = new EngInstruction(inst);
        assertEquals(engInst.giveInstruction(), "VHVHVHVHVGGG");
    }

    @Test
    void giveInstructionFalse() {
        String inst = "LRLRLRLRLFFFO";
        EngInstruction engInst = new EngInstruction(inst);
        assertNotEquals(engInst.giveInstruction(), "VHVHVHVHVGGG");
    }

    @Test
    void giveInstructionWrong() {
        String inst = "LRLRLRLRLFFFO";
        EngInstruction engInst = new EngInstruction(inst);
        assertEquals(engInst.giveInstruction(), "");
    }
}